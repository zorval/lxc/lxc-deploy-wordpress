Mise à jour manuelle de wordpress
=================================

Depuis une installation Debian (paquet **wordpress**)


**NOTE** : À utiliser seulement si vous avez fait l'installation via avec lxc (de ce dépôt)

## Prérequis

* Paquets :
  + `wget`
    - `apt install wget`
* Faire une sauvegarde des répertoires :
  + `/srv/lxc/etc/`
  + `/srv/lxc/var/lib/wordpress/`
  + `/srv/lxc/usr/share/wordpress/`
    - `cp -ra /srv/lxc/etc /srv/lxc/zCP_$(date +%F)_etc`
    - `cp -ra /srv/lxc/var /srv/lxc/zCP_$(date +%F)_var`
    - `cp -ra /srv/lxc/usr /srv/lxc/zCP_$(date +%F)_usr`
* Faire une sauvegarde de la base de donnée
* Désactiver toutes les extentions (plugins)
* Noter le réglage des *permaliens*, voir `<FQDN>/wp-admin/options-permalink.php`


## Télécharger et installer la dernière version

```
# Téléchargement
wget https://wordpress.org/latest.tar.gz -O wordpress_latest_$(date +%F).tar.gz

# Extraction
tar xaf wordpress_latest_$(date +%F).tar.gz

# Déplacement dans le répertoire partagé
mkdir -p /srv/lxc/var/www/
mv wordpress /srv/lxc/var/www/

# Suppression des nouveaux fichiers inutiles
rm -rf /srv/lxc/var/www/wordpress/wp-content
rm -f /srv/lxc/var/www/wordpress/wp-config.php
rm -f /srv/lxc/var/lib/wordpress/wp-content/plugins/{akismet,index.php}
rm -f /srv/lxc/var/lib/wordpress/wp-content/themes/twentynineteen

# Copie des anciens fichiers
cp -a /srv/lxc/usr/share/wordpress/wp-config.php /srv/lxc/var/www/wordpress/wp-config.php
cp -ra /srv/lxc/usr/share/wordpress/wp-content /srv/lxc/var/www/wordpress/
cp -ra /srv/lxc/var/lib/wordpress/wp-content/themes/* /srv/lxc/var/www/wordpress/wp-content/themes/
cp -ra /srv/lxc/var/lib/wordpress/wp-content/plugins/* /srv/lxc/var/www/wordpress/wp-content/plugins/
cp -ra /srv/lxc/var/lib/wordpress/wp-content/uploads/ /srv/lxc/var/www/wordpress/wp-content/


# Création du lien symbolique pour le .htaccess
ln -s /etc/wordpress/htaccess /srv/lxc/var/www/wordpress/.htaccess

# Création du lien symbolique vers /var/www
ln -s /srv/lxc/var/www/wordpress/ /var/www/
```

## Mettre à jour les droits

```
chown -R www-data:www-data /var/www/wordpress/
chmod 440 /var/www/wordpress/wp-config.php
```

## Modifier la conf apache2

Fichier `/etc/apache2/sites-enabled/wp.conf`
+ Remplacer `DocumentRoot` par `/var/www/wordpress`
+ Remplacer `Alias /wp-content` par `/var/www/wordpress/wp-content`
+ Remplacer `Directory /usr/share/wordpress` par `/var/www/wordpress`
+ Supprimer le bloc `Directory /var/lib/wordpress/wp-content`

## Modifier la conf wordpress

+ Modifier le fichier `/etc/wordpress/config-<FQDN>` dans la section `WP_CONTENT_DIR` pour pointer vers `/var/www/wordpress/wp-content`

## Test conf apache2 et reload

+ `apache2ctl configtest`
+ `systemctl reload apache2`

## Lancer la MàJ

+ Aller sur l'URL `<FQDN>/wp-admin/upgrade.php`
+ Vérifier le réglage des *permaliens*, voir `<FQDN>/wp-admin/options-permalink.php`
+ Vérifier les mises à jours des thèmes et pluggins
+ Réactiver les extentions

## Désinstaller wordpress

+ `apt remove wordpress`

Doc officielle : https://wordpress.org/support/article/upgrading-wordpress-extended-instructions
